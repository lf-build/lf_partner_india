** Image Name : Image is not available yet **

#####Availalbe End Points:

1. Add partner information
   - POST 
   - Body (Partner Object)
   
2. Add contact for partner 
   - POST (/{partnerId}/contact)
   - Body (Contact Object)
   
3. Update Contact 
   - PUT (/{partnerId}/contact/{contactId})
   
4. Delete Contact
   - DELETE (/{partnerId}/contact/{contactId})
   
5. Attach Contact Details
    - PUT (/{partnerId}/attach/{contactId})
	
6. Detach Contact User 
   - PUT (/{partnerId}/detach/{contactId}/{userId})
   
7. Get All 
   - GET (/all)

8. Get by Partner Id
   - GET (/{id})
   
9. Delete by Partner Id
   - DELETE (/{id})
   
10. Update Active Contact details
   - PUT (/{partnerId}/active-contact)
   
10. Add Partner User
   - POST (/{partnerId}/users)
   - Body (PartnerUserRequest)
   
10. Assosiate User 
   - PUT (/{partnerId}/users/{userId})

11 Delete Partner User 
   - DELETE (/{partnerId}/users/{userId})
  
#####Configurations :

1. Configuration for the tenant

{
    "paymentTimeLimit": "15",
    "isActive": "True",
    "name": "my-tenant",
    "website": "www.lendfoundry.com",
    "email": "my-tenantX@lendfoundry.com",
    "phone": "anything",
    "timezone": "America/Detroit"
}
